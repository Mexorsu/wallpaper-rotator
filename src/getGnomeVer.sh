#!/bin/bash
if [ "$XDG_CURRENT_DESKTOP" = "" ]
then
    desktop=$(echo "$XDG_DATA_DIRS" | sed 's/.*\(xfce\|kde\|gnome\).*/\1/')
else
    desktop=$XDG_CURRENT_DESKTOP
fi
desktop=${desktop,,}  # convert to lower case

if [[ $desktop != "gnome" ]]
then
    echo "This script works only with gnome desktop! Detected desktop env: $desktop"
    exit 0;
fi
VERSION="1"
GNOME_ABOUT=`which gnome-about`
GNOME_SESSION=`which gnome-session`
# Check for gnome 2 versions
if [[ $GNOME_ABOUT != "" ]]
then
    VERSION=`gnome-about --version | sed -ne 's/Version:[ \t]*\([0-9]\)\.[0-9]\+\.[0-9]\+/\1/p'`
fi
# Check for gnome 3 version
if [[ $GNOME_SESSION != "" ]]
then
    VERSION=`gnome-session --version | sed -e 's/[^0-9]*\([0-9]\)\.[0-9]\+\.[0-9]\+/\1/'`
fi
echo "gnome$VERSION"

