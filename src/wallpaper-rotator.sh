#!/bin/bash

print_usage(){
    echo "Usage:"
    echo "$ wallpaper-rotator config    : Allows to configure timings and wallpapers folder"
    echo "                                in interactive mode. User must pass in valid number"
    echo "                                as <TIMEOUT> in seconds, and valid (existing) folder"
    echo "                                as <PICTURE_FOLDER>. Deafults are 60 and \"~/wallpapers\""
    echo "                                respectively. Configuration is saved in "
    echo "                                \"~/.wallpaper-rotator\" file."
    echo "$ wallpaper-rotator start     : Starts the deamon manually, using \"~/.wallpaper-rotator\" as"
    echo "                                config file, or allowing passing configuration interactively"
    echo "                                if none is set."
    echo "$ wallpaper-rotator stop      : Stops the deamon."
    echo "$ wallpaper-rotator status    : Prints deamon status (configs + is_running?)."
    echo "$ wallpaper-rotator restart   : Restarts the deamon using \"~/.wallpaper-rotator\" file"
    echo "                                (requires valid configuration)."
}

if ! [[ -d /opt/wallpaper-rotator ]]
then
    echo "Error: wallpaper-rotator is not installed"
    echo "  install with ./install.sh first"
    exit 0;
fi

if [[ $# -eq 0 ]]
then
    print_usage
    exit 0;
fi

while [[ $# > 0 ]]
do
key="$1"
case $key in
    stop)
        /opt/wallpaper-rotator/changeWallpaperDaemon.sh -k immediate
        exit 0;
    ;;
    status)
        TIME_INTERVAL_CONFIG=`cat ~/.wallpaper-rotator 2>/dev/null | grep 'changeWallpaperDaemon' | sed -e 's/.*-t \([0-9]\+\).*/\1/'`
        if [[ $TIME_INTERVAL_CONFIG == "" ]]
        then
            TIME_INTERVAL_CONFIG="<not set>"
        fi
        FOLDER_CONFIG=`cat ~/.wallpaper-rotator 2>/dev/null | grep 'changeWallpaperDaemon' | sed -e 's/.*-t [0-9]\+ \(.*\) >.*/\1/'`
        if [[ $FOLDER_CONFIG == "" ]]
        then
            FOLDER_CONFIG="<not set>"
        fi
        IS_RUNNING=`cat ~/.wallpaper_rotator_pid 2>/dev/null | wc -l`
        if [[ $IS_RUNNING == "0" ]]
        then
            IS_RUNNING="no"
        else
            IS_RUNNING="yes"
        fi
        echo "Running?     : $IS_RUNNING"
        echo "Pic Folder   : $FOLDER_CONFIG"
        echo "Time Interval: $TIME_INTERVAL_CONFIG"
    ;;
    start)
        INSTANCE_PID=`cat ~/.wallpaper_rotator_pid 2>/dev/null`
        if [[ $INSTANCE_PID =~ [0-9]+ ]]
        then
            REAL_COUNT=`ps -ef | grep $INSTANCE_PID 2>/dev/null | grep -v grep | wc -l`
        else
            REAL_COUNT="0"
        fi
        if [[ $REAL_COUNT == "0" ]]
        then
            # this pid file is bullshitting us, remove it
            rm -rf ~/.wallpaper_rotator_pid
            touch ~/.wallpaper_rotator_pid
        else
            echo "Instance already running! Stop with \"wallpaper-rotator stop\" or restart with \"wallpaper-rotator restart\": $INSTANCES_COUNT"
            exit 1;
        fi
        START_CMD=`cat ~/.wallpaper-rotator 2>/dev/null | grep 'changeWallpaperDaemon'`
        if [[ $START_CMD != "" ]]
        then
            bash -c "$START_CMD"
            exit 0;
        else
            MODE="CONFIGANDSTART"
        fi
    ;;
    restart)
        /opt/wallpaper-rotator/changeWallpaperDaemon.sh -k immediate 2>/dev/null 1>&2
        START_CMD=`cat ~/.wallpaper-rotator 2>/dev/null | grep 'changeWallpaperDaemon'`
        if [[ $START_CMD == "" ]]
        then
            echo "No active configuration found, set it first by running \"wallpaper-rotate config\""
            exit 1;
        else
            bash -c "$START_CMD"
            exit 0;
        fi
    ;;
    config)
        MODE="CONFIG"
    ;;
    *)
        echo "Wrong argument: $1"
        exit 1;
    ;;
esac
shift
done

if [[ $MODE != "CONFIG" ]] && [[ $MODE != "CONFIGANDSTART" ]]
then
    exit 0;
fi

LOG_FILE="~/wallpaper-rotator.log"
ENTRIES_COUNT=`cat ~/.wallpaper-rotator 2>/dev/null | grep 'changeWallpaperDaemon.*' | wc -l`
HOME_FOLDER=`cd ; pwd`

while ! [[ $TIME_INTERVAL =~ [0-9]+ ]]
do
    read -p "Enter time interval(default: 60): " TIME_INTERVAL
    TIME_INTERVAL=${TIME_INTERVAL:-60}
done

while ! [[ -d $WALLPAPERS_FOLDER ]]
do
    read -p "Enter wallpapers folder(default: ~/wallpapers): " WALLPAPERS_FOLDER
    WALLPAPERS_FOLDER=${WALLPAPERS_FOLDER:-"~/wallpapers"}
    if [[ $WALLPAPERS_FOLDER == "~/wallpapers" ]]
    then
        mkdir 2>/dev/null ~/wallpapers
    fi
    WALLPAPERS_FOLDER=`echo $WALLPAPERS_FOLDER | sed -e "s#~#$HOME_FOLDER#"`
done

CMD="/opt/wallpaper-rotator/changeWallpaperDaemon.sh -t $TIME_INTERVAL $WALLPAPERS_FOLDER > $LOG_FILE 2>&1 &"
# remove old entry/ies
sed -i "/changeWallpaperDaemon/d" ~/.wallpaper-rotator 2>/dev/null
# add a new one
echo $CMD >> ~/.wallpaper-rotator

if [[ $MODE != "CONFIGANDSTART" ]]
then
    exit 0;
else
# restart daemon silently
    /opt/wallpaper-rotator/changeWallpaperDaemon.sh -k immediate 2>/dev/null 1>&2
    bash -c "$CMD"
fi
