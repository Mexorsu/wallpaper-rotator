#!/bin/bash
PICTURES_FOLDER=~/wallpapers
PID_FILE=~/.wallpaper_rotator_pid
ROTATION_INTERVAL=15
while [[ $# > 0 ]]
do
key="$1"
case $key in
    -t|--time)
        if [[ $2 =~ [0-9]+ ]]
        then
            ROTATION_INTERVAL=$2
            shift
        fi
    ;;
    -k|--kill)
        if [[ -f $PID_FILE ]]
        then
            if [[ $2 == "immediate" || $2 == "i" ]]
            then
                ROTATOR_PID=`cat $PID_FILE`
                kill -9 $ROTATOR_PID
            fi
            rm -rf $PID_FILE
        else
            echo "Wallpaper rotator daemon is not running, or pid file in $PID_FILE is corrupted. You will have to kill it manually."
        fi
        exit 0;
    ;;
    -u|--user)
        AS_USER=$2
        shift
    ;;
    *)
        if [[ -d $1 ]]
        then
            PICTURE_COUNT=`find $1 -type f -regex ".*\.jpe?g" -o -name '*.gif' | wc -l`
            if [[ $PICTURE_COUNT -eq 0 ]]
            then
                echo "$1 contains no pictures!"
                exit 0;
            else
                PICTURES_FOLDER=$1
            fi
        else
            echo "Error: $1 is not a folder!"
            exit 0;
        fi
    ;;
esac
shift
done
if [[ -f $PID_FILE ]]
then
    THE_PID=`cat $PID_FILE`
    PROCESS_COUNT=`ps -ef | grep $THE_PID | grep -v grep | wc -l`
    if [[ $PROCESS_COUNT != "0" ]]
    then
        echo "Wallpaper rotator daemon already running! PID is $THE_PID"
        exit 0
    fi

fi
echo $$ > $PID_FILE
echo "--- Wallpaper rotator started with PID: $$ ---"
while [[ -f $PID_FILE ]]
do
    if [ -z "$AS_USER" ]
    then
       /opt/wallpaper-rotator/changeWallpaper.sh $PICTURES_FOLDER
    else
        su $AS_USER /opt/wallpaper-rotator/changeWallpaper.sh $PICTURES_FOLDER
    fi
    sleep $ROTATION_INTERVAL
done
echo "--- Wallpaper rotator shut down ---"
