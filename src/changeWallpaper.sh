#!/bin/bash
if [[ $# -ne 0 ]]
then
    FOLDER=$1
else
    FOLDER=~/wallpapers
fi
cd $FOLDER
FOLDER=`pwd`
PICS_LEFT=`cat .pics_list | wc -l`
if [[ $PICS_LEFT < 1 ]]
then
    find . -type f -regex ".*\.jpe?g" -o -name '*.gif' | sed -e 's#\./##' | uniq > .pics_list
fi

rm -f 2>/dev/null .pics_list_new
touch .pics_list_new
SWITCH="false"
while read LINE
do
    if [[ $SWITCH == "false" ]]
    then
        SWITCH="true"
        NEW_PICTURE=$LINE
    else
        echo $LINE >> .pics_list_new
    fi
done < .pics_list
mv -f .pics_list_new .pics_list
FILE_URI="file://${FOLDER}/${NEW_PICTURE}"
THE_USER=`whoami`
echo "Changing background to: $FILE_URI"
DBUS_PROCESSES_COUNT=`ps -ef | grep dbus | grep $THE_USER | grep -v grep | wc -l`
if [[ $DISPLAY == "" ]]
then
    DISPLAY=":0"
fi
GNOME_VERSION=`/opt/wallpaper-rotator/getGnomeVer.sh`
if [[ $GNOME_VERSION == "gnome3" ]]
then
    GNOME_CMD="gsettings set org.gnome.desktop.background picture-uri '$FILE_URI'"
elif [[ $GNOME_VERSION == "gnome2" ]]
then
    GNOME_CMD="gconftool-2 --type=string --set /desktop/gnome/background/picture_filename ${FOLDER}/${NEW_PICTURE}"
elif [[ $GNOME_VERSION == "gnome1" ]]
then
    GNOME_CMD="gconftool --type=string --set /desktop/gnome/background/picture_filename ${FOLDER}/${NEW_PICTURE}"
else
    echo "Unrecognised version: $GNOME_VERSION, trying with gsettings"
    GNOME_CMD="gsettings set org.gnome.desktop.background picture-uri '$FILE_URI'"
fi

if [[ $DBUS_PROCESSES_COUNT == "0" ]]
then
    echo "dbus not started, initializing..."
    gsettings set org.gnome.desktop.background picture-uri "$FILE_URI"
else
    gsettings set org.gnome.desktop.background picture-uri "$FILE_URI"
fi
#/home/mexorsu/Obrazy/desktop_slideshow/matrixnoguys.jpg
