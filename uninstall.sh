#!/bin/bash
echo "--- Stopping daemon ---"
/opt/wallpaper-rotator/changeWallpaperDaemon.sh -k immediate 2>/dev/null
echo "--- Removing binaries ---"
sudo rm -rf /opt/wallpaper-rotator
echo "--- removing entry/ies from ~/.bashrc ---"
cp ~/.bashrc ~/.bashrc_bak
#sed -i "/changeWallpaperDaemon/d" ~/.bashrc
rm -rf ~/.wallpaper-rotator
rm -rf ~/.wallpaper-rotator.xpm
rm -rf ~/.local/share/applications/wallpaper-rotator.desktop
echo "--- Removing /usr/bin/wallpaper-rotator ---"
sudo rm -rf /usr/bin/wallpaper-rotator
echo "--- removing additional links from /usr/bin ---"
sudo rm -rf 2>/dev/null /usr/bin/changeWallpaper
sudo rm -rf 2>/dev/null /usr/bin/changeWallpaperDaemon
