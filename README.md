wallpaper-rotator
=================

This "app" is a small collection of scripts that allows easy setting up of wallpaper slideshow in gnome. Tested on gnome3, works in unity, should also work on gnome2. It shouldn't need a lot of tweaking to support mate so feel free to fork and add a few missing lines.

Installation:
=============

To install wallpaper-rotator just type:
---------------------------------------

    $ git clone http://bitbucket.org/mexorsu/wallpaper-rotator

    $ cd wallpaper-rotator

    $ ./install.sh

To un-install, type:
--------------------

    $ git clone http://bitbucket.org/mexorsu/wallpaper-rotator

    $ cd wallpaper-rotator

    $ ./uninstall.sh

Useful:
-------

To restart your wallpaper slideshow automatically when you log in:

###1.) Unity:

  a.) Run:

    $ gnome-session-properties

  b.) Add new entry to "Additional startup programs" with following command:

    wallpaper-rotator restart
### 2.) Gnome3

  a.) run:

    $gnome-tweak-tool
  b.) In "Startup applications" tab, add new entry and choose "wallpaper-rotator" app

Usage:
======

    $ wallpaper-rotator config    : Allows to configure timings and wallpapers folder
                                    in interactive mode. User must pass in valid number
                                    as <TIMEOUT> in seconds, and valid (existing) folder
                                    as <PICTURE_FOLDER>. Deafults are 60 and "~/wallpapers"
                                    respectively. Configuration is saved in
                                    "~/.wallpaper-rotator" file.
    $ wallpaper-rotator start     : Starts the deamon manually, using "~/.wallpaper-rotator" as
                                    config file, or allowing passing configuration interactively
                                    if none is set.
    $ wallpaper-rotator stop      : Stops the deamon.
    $ wallpaper-rotator status    : Prints deamon status (configs + is_running?).
    $ wallpaper-rotator restart   : Restarts the deamon using "~/.wallpaper-rotator" file
                                    (requires valid configuration).

